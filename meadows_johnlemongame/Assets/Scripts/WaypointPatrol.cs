﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;

    public GameObject player;

    public void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");//Find the player and save a reference to them that we can use throughout the game

    }

    public void Update()
    {
        navMeshAgent.SetDestination(player.transform.position);//Constantly keep updating my destination to wherever the player has moved to
    }

}


